
const routes = [
  // Default route
  { path: '', redirect: '/oils' },

  // Dialogs
  { path: '/settings', component: () => import('src/dialogs/Settings') },
  { path: '/receipe/new', component: () => import('src/dialogs/ReceipeEditor') },
  { path: '/receipe/:id/edit', component: () => import('src/dialogs/ReceipeEditor'), props: (route) => ({ id: route.params.id }) },
  { path: '/receipe/:id/make', component: () => import('src/dialogs/BatchCreator'), props: (route) => ({ id: route.params.id }) },

  // Navigation lists
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [

      // Receipes routes
      {
        path: 'receipes',
        component: () => import('pages/ReceipesIndex.vue'),
        children: [
          { path: ':id', component: () => import('pages/ReceipeDetails.vue'), props: (route) => ({ id: route.params.id }) }
        ]
      },

      // Oils routes
      {
        path: 'oils',
        component: () => import('pages/OilsIndex.vue'),
        children: [
          { path: ':id', component: () => import('pages/OilDetails.vue'), props: (route) => ({ id: route.params.id }) }
        ]
      },

      // Batches routes
      {
        path: 'batches',
        component: () => import('pages/BatchesIndex.vue'),
        children: [
          { path: ':id', component: () => import('pages/BatchDetails.vue'), props: (route) => ({ id: route.params.id }) }
        ]
      }

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
