function listTransaction (db, storeName) {
  return new Promise(resolve => {
    const trans = db.transaction([storeName], 'readonly')
    trans.oncomplete = () => {
      resolve(result)
    }

    const store = trans.objectStore(storeName)
    const result = []

    store.openCursor().onsuccess = e => {
      const cursor = e.target.result
      if (cursor) {
        result.push(cursor.value)
        cursor.continue()
      }
    }
  })
}

function rwTransaction (db, storeName, executor) {
  return new Promise(resolve => {
    const trans = db.transaction([storeName], 'readwrite')
    trans.oncomplete = () => {
      resolve()
    }

    const store = trans.objectStore(storeName)
    executor(store)
  })
}

export { rwTransaction, listTransaction }
