export default {

  props: {
    data: {
      type: Object,
      default: () => {}
    }
  },

  model: {
    prop: 'data',
    event: 'dataUpdated'
  },

  methods: {

    updateData (newData) {
      this.$emit('dataUpdated', newData)
    },

    updateField (name, value) {
      const fieldData = {}
      fieldData[name] = value
      this.updateData(Object.assign({}, this.data, fieldData))
    },

    updateNumericField (name, value) {
      this.updateField(name, parseFloat(value))
    },

    insertListItem (fieldName, value) {
      this.updateField(fieldName, [...this.data[fieldName], value])
    },

    deleteListItem (fieldName, idx) {
      const newArray = [...this.data[fieldName]]
      newArray.splice(idx, 1)
      this.updateField(fieldName, newArray)
    },

    updateListItem (fieldName, idx, newValue) {
      const newArray = [...this.data[fieldName]]
      newArray[idx] = newValue
      this.updateField(fieldName, newArray)
    },

    validate () {
      return true
    }

  }

}
