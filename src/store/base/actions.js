import { v5 as uuidv5 } from 'uuid'

import DB from 'src/tools/DB.js'

async function _initStore (commit, translator) {
  let res = {}
  res = await DB.listOils()
  const oils = {}
  res.forEach(oil => {
    if (oil.inci) {
      oil.name = translator(oil.inci)
    }

    oils[oil.id] = oil
  })
  commit('initOils', oils)

  res = await DB.listReceipes()
  const receipes = {}
  res.forEach(receipe => {
    receipes[receipe.id] = receipe
  })
  commit('initReceipes', receipes)

  res = await DB.listBatches()
  const batches = {}
  res.forEach(batch => {
    batches[batch.id] = batch
  })
  commit('initBatches', batches)
}

export function initStore ({ commit }, translator) {
  return _initStore(commit, translator)
}

export function updateOilsTranslations ({ commit }, translator) {
  commit('updateOilsTranslations', translator)
}

export function addReceipe ({ commit }, receipe) {
  return new Promise((resolve, reject) => {
    const uuid = uuidv5(receipe.name, uuidv5.URL)
    receipe = Object.assign(receipe, { id: uuid })

    DB.putReceipe(receipe).then(() => {
      commit('addReceipe', receipe)
      resolve()
    })
  })
}

export function updateReceipe ({ commit }, receipe) {
  return new Promise((resolve, reject) => {
    const uuid = uuidv5(receipe.name, uuidv5.URL)
    receipe = Object.assign(receipe, { id: uuid })

    DB.putReceipe(receipe).then(() => {
      commit('updateReceipe', receipe)
      resolve()
    })
  })
}

export function deleteReceipe ({ commit }, name) {
  return new Promise((resolve, reject) => {
    DB.deleteReceipe(name).then(() => {
      commit('deleteReceipe', name)
      resolve()
    })
  })
}

export function addBatch ({ commit }, batch) {
  return new Promise((resolve, reject) => {
    const uuid = uuidv5(batch.name, uuidv5.URL)
    batch = Object.assign(batch, { id: uuid })

    DB.putBatch(batch).then(() => {
      commit('addBatch', batch)
      resolve()
    })
  })
}

export function updateBatch ({ commit }, batch) {
  return new Promise((resolve, reject) => {
    const uuid = uuidv5(batch.name, uuidv5.URL)
    batch = Object.assign(batch, { id: uuid })

    DB.putBatch(batch).then(() => {
      commit('updateBatch', batch)
      resolve()
    })
  })
}

export function deleteBatch ({ commit }, name) {
  return new Promise((resolve, reject) => {
    DB.deleteBatch(name).then(() => {
      commit('deleteBatch', name)
      resolve()
    })
  })
}
