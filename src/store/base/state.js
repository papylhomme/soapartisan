export default function () {
  return {
    fattyAcids: {
      lauric: { name: 'lauric', color: '#4E79A7' },
      myristic: { name: 'myristic', color: '#F28E2B' },
      palmitic: { name: 'palmitic', color: '#E15759' },
      stearic: { name: 'stearic', color: '#76B7B2' },
      ricinoleic: { name: 'ricinoleic', color: '#59A14F' },
      oleic: { name: 'oleic', color: '#EDC948' },
      linoleic: { name: 'linoleic', color: '#B07AA1' },
      linolenic: { name: 'linolenic', color: '#FF9DA7' }
    },

    oils: {},
    receipes: {},
    batches: {}
  }
}
