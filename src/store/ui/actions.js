export function discardInfoBanner ({ commit }, id) {
  commit('discardInfoBanner', id)
}

export function resetInfoBanners ({ commit }) {
  commit('resetInfoBanners')
}
